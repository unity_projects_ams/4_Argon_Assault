﻿using UnityEngine;
using UnityEngine.SceneManagement; // ok when only one script is loaded

public class CollisionHandler : MonoBehaviour {

    [SerializeField] float levelLoadDelay = 1f;
    [Tooltip("FX prefab on player")][SerializeField] GameObject deathFx;

    void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
    }

    private void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");
        deathFx.SetActive(true);
        Invoke("ReloadScene", levelLoadDelay);
    }

    private void ReloadScene() // string reference
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
