﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour
{

    void Awake()
    {
        int numOfMusicPlayers = FindObjectsOfType<MusicPlayer>().Length;
        // if more than one player, destroy it
        if (numOfMusicPlayers > 1)
        {
            Destroy(this.gameObject);
        } else 
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
